#Feature Definition Template
Feature: Apptus Test Case Automation

  @Test1
  Scenario Outline: Apptus Test Case Positive Flow Automation
    Given Log into the Catfull environment and click + button on the menu bar
    When Service Agreements is selected and new button is clicked
    Then Select Vendor Service Agreement from the "<TestData>" and click Continue button
    Then Fill the Agreement with "<TestData>"
    Then Verify the Agreement Data with the "<TestData>"

    Examples: 
      | TestData                         |
      | Vendor Service Agreement layouts |
      | Vendor Consulting SOW layouts    |
      | Vendor SOW layouts               |

  @Test2
  Scenario Outline: Verfiy All The Fields Are Available or Not
    Given Log into the Catfull environment and click + button on the menu bar
    When Service Agreements is selected and new button is clicked
    Then Select "<TestData>" Agreement
    Then verify All the Fields

    Examples: 
      | TestData |
      |          |
