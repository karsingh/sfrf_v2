#Feature Definition Template
Feature: L2 Triage Routed App

  @Test3
  Scenario Outline: Route VMWare App request to US L2 Triage
    Given Login to catfull environment and navigate to RemedyForce self service
    When I click on Request Support with an Applications(US & JP)
    Then user enter mandatory details "<TestData>"
    Then user click on submit
    Then user select to stay on the page option
    Then user search the current ticket in search bar
    Then verfiy the ticket belongs to right "<TestData>" queue or not

    Examples: 
      | TestData                                          |
      | VPN App request to US L2 Triage                   |
      | VMWare App request to US L2 Triage                |
      | CEDW App request to US L2 Triage                  |
      | Big Insights App request to US L2 Triage          |
      | Request Support with an Application US            |
      | Category Availability App request to US L2 Triage |
      | Campaign Design App request to US L2 Triage       |
      | TestLink App request to US L2 Triage              |
      | Gemini App request to US L2 Triage                |
      | Concur App request to US L2 Triage                |
      | RNS Field Service ID App request to US L2 Triage  |
      | EU VDI App request to US L2 Triage                |
      | Alliance App request to US L2 Triage              |
      | Apttus option in Self Service menu                |
      | Apttus option in Self Service menu(JP)            |

  @Test4
  Scenario: Test Folder and Files
    Given Login to catfull environment and navigate to RemedyForce self service
    Then click on New
    Then click on category and verfiy
