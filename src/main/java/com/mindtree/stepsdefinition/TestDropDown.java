package com.mindtree.stepsdefinition;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.TestDropDownUiStore;

import cucumber.api.java.en.Then;

public class TestDropDown {

	WebDriver driver;
	TestDropDownUiStore uiStore;
	HelperClass helper = new HelperClass();
	WebElement element;

	@Then("^click on New$")
	public void click_on_New() {
		driver = L2TriageRoutedStepdefinition.driver;
		uiStore = new TestDropDownUiStore(driver);
		driver.findElement(uiStore.clickonDropDown).click();
		driver.findElement(uiStore.clickRemedyConsole).click();
		driver.findElement(uiStore.clickNew).click();
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.switchTo().frame("iframe-");
		WebElement iframe = driver.findElement(By.xpath(
				"//iframe[@src='/apex/ConsoleIncidentDetail?clientId=&inctype=Incident&ObjID=&ObjName=&isRFConsoleDetailForm=true&formLayoutId=&templateId=&isLayoutChanged=false']"));
		driver.switchTo().frame(iframe);
		element = helper.waitForElement(uiStore.clickCategory);
		element.click();
		ArrayList<String> tab3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab3.get(1));
		driver.manage().window().maximize();
	}

	@Then("^click on category and verfiy$")
	public void click_on_category_and_verfiy() {

	}

}
