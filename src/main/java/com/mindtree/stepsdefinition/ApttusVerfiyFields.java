package com.mindtree.stepsdefinition;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.mindtree.pagemethods.ApttusVerifyFieldsMethod;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.ApttusPositiveFlowUistore;

import cucumber.api.java.en.Then;

public class ApttusVerfiyFields {
	ApttusVerifyFieldsMethod methods = new ApttusVerifyFieldsMethod();

	HelperClass helper=new HelperClass();
	
	WebDriver driver;

	ApttusPositiveFlowUistore uiStore = new ApttusPositiveFlowUistore(ApttusPositiveFlow.driver);

	@Then("^Select \"([^\"]*)\" Agreement$")
	public void select_Agreement(String arg1) throws IOException {
		driver = methods.chooseAgreement(arg1, ApttusPositiveFlow.driver);
		driver.findElement(uiStore.continueFirst).click();
		driver.findElement(uiStore.continueSecond).click();
	}

	@Then("^verify All the Fields$")
	public void verify_All_the_Fields() {
      try {
		driver=methods.dropDownCheck(driver);
		driver=methods.verify("General Information", driver);
		driver=methods.termTermination("Term and Termination", driver);
		driver=methods.paymentTerm("Payment and Pricing", driver);
		driver=methods.litigation("Conflict Resolution", driver);
		driver=methods.indemnificationScope("Liability and Indemnification", driver);
		driver=methods.specialDamage("Damages", driver);
		driver=methods.miscellaneous("Miscellaneous", driver);
		driver=methods.signatures("Signatures", driver);
		driver=methods.systemInformation("System Information", driver);
	} catch (IOException e) {
		e.printStackTrace();
	}
      helper.closeUrl(driver);
	}

}
