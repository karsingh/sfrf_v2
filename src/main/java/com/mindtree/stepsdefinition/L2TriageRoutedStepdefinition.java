package com.mindtree.stepsdefinition;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.pagemethods.L2TriageRoutedMethods;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.L2TriageRoutedFillDetailsUistore;
import com.mindtree.uistore.L2TriageRoutedUistore;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author M1048963 date:2/14/2019 contains step definitions
 *
 */
public class L2TriageRoutedStepdefinition {

	static WebDriver driver;

	HelperClass helper = new HelperClass();

	L2TriageRoutedUistore uiStore;
	
	L2TriageRoutedFillDetailsUistore fillDetailsuiStore;

	L2TriageRoutedMethods pageMethods;

	WebElement element;
	
	@Given("^Login to catfull environment and navigate to RemedyForce self service$")
	public void login_to_catfull_environment_and_navigate_to_RemedyForce_self_service() throws IOException, InterruptedException {

		Properties property = helper.propertyConfig();
		String url = property.getProperty("url");
		driver = helper.startBrowser(url);
		uiStore = new L2TriageRoutedUistore(driver);
		pageMethods = new L2TriageRoutedMethods(driver);
		fillDetailsuiStore=new L2TriageRoutedFillDetailsUistore(driver);
		String username = property.getProperty("username");
		String password = property.getProperty("password");
		driver = pageMethods.login(username, password);
		Thread.sleep(10000);
		
	}

	@When("^I click on Request Support with an Applications\\(US & JP\\)$")
	public void i_click_on_Request_Support_with_an_Applications_US_JP() throws InterruptedException {
		driver.switchTo().frame("myFrameOld");
		driver.findElement(uiStore.requestSupportUs).click();
		
	}

	@Then("^user enter mandatory details \"([^\"]*)\"$")
	public void user_enter_mandatory_details(String arg1) throws IOException, InterruptedException  {
		WebElement iframe=driver.findElement(By.xpath("//iframe[@src='/apex/bmcservicedesk__SelfServiceIncidentCustom?isServiceRequest=true&reqDefId=a5ZF00000000A3xMAE']"));
		driver.switchTo().frame(iframe);
		pageMethods.fillDetails(arg1, driver);

	}

	@Then("^user click on submit$")
	public void user_click_on_submit() {
		driver.findElement(fillDetailsuiStore.submit).click();

	}

	@Then("^user select to stay on the page option$")
	public void user_select_to_stay_on_the_page_option() {
		WebElement element=helper.waitForElement(fillDetailsuiStore.alert);
		element.findElement(fillDetailsuiStore.alert).click();
	}
	@Then("^user search the current ticket in search bar$")
	public void user_search_the_current_ticket_in_search_bar() {
		driver.switchTo().parentFrame();
		String ticket=driver.findElement(fillDetailsuiStore.tickedNumber).getText();
		ticket=pageMethods.ticketNumber(ticket);
		driver.switchTo().parentFrame();
		driver=pageMethods.clickSearch(ticket, driver);
		driver.findElement(By.linkText(ticket)).click();
	}
	@Then("^verfiy the ticket belongs to right \"([^\"]*)\" queue or not$")
	public void verfiy_the_ticket_belongs_to_right_queue_or_not(String arg1) {
	   try {
		driver=pageMethods.verifyQueue(driver, arg1);
	} catch (IOException e) {
		System.out.println(e.getStackTrace());
	}
	   helper.closeUrl(driver);
	}

}
