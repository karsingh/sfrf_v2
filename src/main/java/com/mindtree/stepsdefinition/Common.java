package com.mindtree.stepsdefinition;

import cucumber.api.Scenario;
import cucumber.api.java.After;

public class Common {
	public Scenario scenario;
	
	 @After
	    public void beforeScenario(Scenario scenarioObj){
	       this.scenario=scenarioObj;
	       System.out.println(scenarioObj.getId() + " " +scenarioObj.getStatus());
	}

}
