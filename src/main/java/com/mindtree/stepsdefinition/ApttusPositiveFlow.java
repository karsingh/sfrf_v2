package com.mindtree.stepsdefinition;

import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mindtree.pagemethods.ApttusPositiveFlowMethods;
import com.mindtree.pagemethods.ApttusPositiveFlowVerifyMethods;
import com.mindtree.pagemethods.L2TriageRoutedMethods;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.ApttusPositiveFlowUistore;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApttusPositiveFlow {

	HelperClass helper = new HelperClass();

	static WebDriver driver;

	L2TriageRoutedMethods pageMethods;

	WebElement element;

	ApttusPositiveFlowUistore uiStore;

	ApttusPositiveFlowMethods apttusPageMethods;
	
	ApttusPositiveFlowVerifyMethods apttusverfiy;

	@Given("^Log into the Catfull environment and click \\+ button on the menu bar$")
	public void log_into_the_Catfull_environment_and_click_button_on_the_menu_bar()
			throws IOException, InterruptedException {
		Properties property = helper.propertyConfig();
		String url = property.getProperty("url");
		driver = helper.startBrowser(url);
		pageMethods = new L2TriageRoutedMethods(driver);
		uiStore = new ApttusPositiveFlowUistore(driver);
		apttusPageMethods = new ApttusPositiveFlowMethods(driver);
		apttusverfiy=new ApttusPositiveFlowVerifyMethods(driver);
		String username = property.getProperty("username");
		String password = property.getProperty("password");
		driver = pageMethods.login(username, password);
		Thread.sleep(10000);
	}

	@When("^Service Agreements is selected and new button is clicked$")
	public void service_Agreements_is_selected_and_new_button_is_clicked() {
		driver.findElement(uiStore.clickonDropDown).click();
		driver.findElement(uiStore.apttusMang).click();
		driver.findElement(uiStore.clickNew).click();
	}

	@Then("^Select Vendor Service Agreement from the \"([^\"]*)\" and click Continue button$")
	public void select_Vendor_Service_Agreement_from_the_and_click_Continue_button(String arg1) {
		try {
			driver = apttusPageMethods.chooseAgreement(arg1, driver);
			driver.findElement(uiStore.continueFirst).click();
			driver.findElement(uiStore.continueSecond).click();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Then("^Fill the Agreement with \"([^\"]*)\"$")
	public void fill_the_Agreement_with(String arg1) throws IOException {
		driver = apttusPageMethods.fillDetails(arg1, driver);
	}

	@Then("^Verify the Agreement Data with the \"([^\"]*)\"$")
	public void verify_the_Agreement_Data_with_the(String arg1) throws IOException {
       try {
		driver=apttusverfiy.verifyDetails(arg1, driver);
	} catch (ParseException e) {
		e.printStackTrace();
	}
       helper.closeUrl(driver);
	}

}
