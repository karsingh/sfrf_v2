package com.mindtree.pagemethods;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.ApttusPositiveFlowFillDetailsUistore;
import com.mindtree.uistore.ApttusPositiveFlowUistore;
import com.mindtree.uistore.ApttusPositiveFlowVerifyuiStore;
import com.mindtree.utility.ExcelReadWrite;

public class ApttusPositiveFlowVerifyMethods {

	HelperClass helper = new HelperClass();

	WebDriver driver;

	ExcelReadWrite excelData = new ExcelReadWrite();

	ApttusPositiveFlowUistore uiStore = new ApttusPositiveFlowUistore(driver);

	ApttusPositiveFlowVerifyuiStore uiVerifyStore = new ApttusPositiveFlowVerifyuiStore(driver);

	ApttusPositiveFlowFillDetailsUistore uiFillStore = new ApttusPositiveFlowFillDetailsUistore(driver);

	public WebDriver verifyDetails(String key, WebDriver driver) throws IOException, ParseException {
		HashMap<String, String> map = new HashMap<String, String>();

		excelData.readTestData("Apttus");

		if (ExcelReadWrite.map.containsKey(key)) {
			map = ExcelReadWrite.map.get(key);
			ApttusPositiveFlowVerifyMethods obj = new ApttusPositiveFlowVerifyMethods(driver);
			if (key.equalsIgnoreCase("Vendor Consulting SOW layouts")) {
				verify(driver, map, obj);
			} else {
				obj.checkDetails(uiVerifyStore.productName, map.get("Product Name"), driver);
				verify(driver, map, obj);
			}
		}

		return driver;

	}

	private void verify(WebDriver driver, HashMap<String, String> map, ApttusPositiveFlowVerifyMethods obj)
			throws ParseException {
		obj.checkDetails(uiVerifyStore.vendorNotes, map.get("Vendor Notes"), driver);
		obj.checkDetails(uiVerifyStore.account, map.get("Account"), driver);
		obj.checkDetails(uiVerifyStore.vendorContact, map.get("Vendor Contact Information"), driver);
		obj.checkDetails(uiVerifyStore.internalNotes, map.get("Internal Notes"), driver);
		String agreementValue = driver.findElement(uiVerifyStore.totalAgreementValue).getText();
		agreementValue = agreementValue.replaceAll("[^\\d]", "");
		if (agreementValue.equalsIgnoreCase(map.get("Total Agreement Value"))) {
			Reporter.addStepLog("The value entered with " + map.get("Total Agreement Value") + " is correct");
			System.out.println("The value entered with " + map.get("Total Agreement Value") + " is correct");
		} else {
			Reporter.addStepLog("The value entered with " + map.get("Total Agreement Value") + " is not correct");
		}
		String contract = driver.findElement(uiVerifyStore.totalAgreementValue).getText();
		contract = contract.replaceAll("[^\\d]", "");
		if (contract.equalsIgnoreCase(map.get("Contract Total Year # 1"))) {
			Reporter.addStepLog("The value entered with " + map.get("Contract Total Year # 1") + " is correct");
			System.out.println("The value entered with " + map.get("Contract Total Year # 1") + " is correct");
		} else {
			Reporter.addStepLog("The value entered with " + map.get("Contract Total Year # 1") + " is not correct");
		}
		obj.checkDetails(uiVerifyStore.numberofMilestone, map.get("Number of Milestones"), driver);
		obj.checkDetails(uiVerifyStore.quantity, map.get("Quantity"), driver);
		obj.checkDetails(uiVerifyStore.costCenter, map.get("Cost Center Manager"), driver);
		String startDate = driver.findElement(uiVerifyStore.startDate).getText();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyy");
		Date date = format.parse(startDate);
		String newdate = format.format(date);
		if (newdate.equalsIgnoreCase(helper.getDate(Integer.parseInt(map.get("Agreement Start Date"))))) {
			Reporter.addStepLog("The value entered with " + newdate + " is correct");
			System.out.println("The value entered with " + newdate + " is correct");
		} else
			Reporter.addStepLog("The value entered with " + newdate + " is not correct");
		String endDate = driver.findElement(uiVerifyStore.endDate).getText();
		Date finaldate = format.parse(endDate);
		String newenddate = format.format(finaldate);
		if (newenddate.equalsIgnoreCase(helper.getDate(Integer.parseInt(map.get("Agreement End Date"))))) {
			Reporter.addStepLog("The value entered with " + newenddate + " is correct");
			System.out.println("The value entered with " + newenddate + " is correct");
		} else
			Reporter.addStepLog("The value entered with " + newenddate + " is not correct");
	}

	public ApttusPositiveFlowVerifyMethods(WebDriver driver) {
		this.driver = driver;
	}

	public void checkDetails(By item, String value, WebDriver driver) {
		String text = driver.findElement(item).getText();
		if (text.equalsIgnoreCase(value)) {
			Reporter.addStepLog("The value entered with " + value + " is correct");
			System.out.println("The value entered with " + value + " is correct");
		} else {
			Reporter.addStepLog("The value entered with " + value + " is not correct");
		}

	}

}
