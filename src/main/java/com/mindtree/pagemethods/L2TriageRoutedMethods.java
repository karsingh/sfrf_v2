package com.mindtree.pagemethods;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.stepsdefinition.Common;
import com.mindtree.uistore.L2TriageRoutedFillDetailsUistore;
import com.mindtree.uistore.L2TriageRoutedUistore;

import com.mindtree.utility.ExcelReadWrite;

/**
 * @author M1048963 methods of every page
 */
public class L2TriageRoutedMethods {

	WebDriver driver;

	L2TriageRoutedUistore uiStore = new L2TriageRoutedUistore(driver);

	L2TriageRoutedFillDetailsUistore fillDetailsuiStore = new L2TriageRoutedFillDetailsUistore(driver);

	ExcelReadWrite excelData = new ExcelReadWrite();

	Common common = new Common();

	HelperClass helper = new HelperClass();

	public L2TriageRoutedMethods(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebDriver login(String username, String password) {
		driver.findElement(uiStore.username).sendKeys(username);
		driver.findElement(uiStore.password).sendKeys(password);
		driver.findElement(uiStore.login).click();
		return driver;
	}

	public WebDriver fillDetails(String key, WebDriver driver) throws IOException {
		HashMap<String, String> map = new HashMap<String, String>();

		List<String> list;

		excelData.readTestData("Remedy Force");

		if (ExcelReadWrite.map.containsKey(key)) {
			map = ExcelReadWrite.map.get(key);

			list = helper.dropDown(fillDetailsuiStore.country, driver);

			if (list.contains(map.get("Country"))) {
				helper.insertValue(fillDetailsuiStore.country, driver, map.get("Country"));

				list = helper.dropDown(fillDetailsuiStore.application, driver);

				if (list.contains(map.get("Specify Application"))) {
					helper.insertValue(fillDetailsuiStore.application, driver, map.get("Specify Application"));

					list = helper.dropDown(fillDetailsuiStore.support, driver);

					if (list.contains(map.get("Type of Support needed"))) {
						helper.insertValue(fillDetailsuiStore.support, driver, map.get("Type of Support needed"));

						driver.findElement(fillDetailsuiStore.description).sendKeys(map.get("Request Description"));
					} else
						System.out.println("The Requried data support is not present in drop down");

				} else {
					System.out.println("The Requried data application support is not present in drop down");
				}

			} else
				System.out.println("The Requried data is not present in drop down");
		}

		return driver;

	}

	public String ticketNumber(String str) {
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
		Matcher matcher = pattern.matcher(str);
		while (matcher.find()) {
			String temp = matcher.group();
			str = str.replaceAll("\\" + temp, "");
		}
		return str;
	}

	public WebDriver clickSearch(String str, WebDriver driver) {
		driver.findElement(fillDetailsuiStore.searchBox).click();
		;
		driver.findElement(fillDetailsuiStore.searchBox).sendKeys(str);
		driver.findElement(fillDetailsuiStore.clickSearchBox).click();
		return driver;

	}

	public WebDriver verifyQueue(WebDriver driver, String key) throws IOException {
		String str = driver.findElement(fillDetailsuiStore.queue).getText();
		HashMap<String, String> map = new HashMap<String, String>();

		excelData.readTestData("Remedy Force");

		map = ExcelReadWrite.map.get(key);

		Assert.assertEquals(str, map.get("Expected Queue"));

		if (str.equalsIgnoreCase(map.get("Expected Queue"))) {
			System.out.println("Expected queue and queue assigned both are same for this " + key + " test case");
		} else {
			System.out.println("Expected queue and queue assigned both are not same for this " + key + " test case");
		}
		return driver;
	}

}
