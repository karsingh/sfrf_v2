package com.mindtree.pagemethods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.ApttusPositiveFlowUistore;
import com.mindtree.uistore.ApttusVerifyFieldsUiStore;
import com.mindtree.utility.ExcelReadWrite;

public class ApttusVerifyFieldsMethod {

	HelperClass helper = new HelperClass();

	WebDriver driver;

	ExcelReadWrite excelData = new ExcelReadWrite();

	ApttusPositiveFlowUistore uiStore = new ApttusPositiveFlowUistore(driver);

	ApttusVerifyFieldsUiStore uiVerifyStore = new ApttusVerifyFieldsUiStore(driver);

	public WebDriver chooseAgreement(String key, WebDriver driver) throws IOException {

		List<String> list = new ArrayList<String>();

		list = helper.dropDown(uiStore.agreementRecord, driver);

		if (list.contains("Manufacturer Master Agreement")) {
			helper.insertValue(uiStore.agreementRecord, driver, "Manufacturer Master Agreement");
			Reporter.addStepLog("The Requried data to Select Agreement Record Type is selected");
		} else {
			Reporter.addStepLog("The Requried data to Select Agreement Record Type is not present in drop down");
		}
		return driver;
	}

	public WebDriver dropDownCheck(WebDriver driver) throws IOException {
		List<String> dropDown = new ArrayList<String>();

		List<String> data = new ArrayList<String>();

		dropDown = helper.dropDown(uiVerifyStore.statusCategory, driver);

		excelData.readData("sheet1");

		data = ExcelReadWrite.list.get("Drop Down Value");

		dropDown.remove(0);
		System.out.println(dropDown.equals(data));
		if (dropDown.equals(data)) {
			for (int i = 0; i < dropDown.size(); i++) {
				List<String> temp = new ArrayList<String>();
				String str = dropDown.get(i);
				driver = helper.insertValue(uiVerifyStore.statusCategory, driver, str);
				temp = helper.dropDown(uiVerifyStore.status, driver);
				data = ExcelReadWrite.list.get(str);
				temp.remove(0);
				if (temp.equals(data)) {
					System.out.println("The value with " + str + " is present in second drop down");
					Reporter.addStepLog("The value with " + str + " is present in second drop down");
				} else {
					System.out.println("The value with " + str + " is not present in second drop down");
					Reporter.addStepLog("The value with " + str + " is not present in second drop down");
				}
			}
		} else {
			System.out.println("The value is not present in first drop down");
			Reporter.addStepLog("The value is not present in first drop down");
		}

		return driver;

	}

	public WebDriver verify(String key, WebDriver driver) throws IOException {
		List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.agreementName, data, driver);
		obj.check(uiVerifyStore.account, data, driver);
		obj.check(uiVerifyStore.recordType, data, driver);
		obj.check(uiVerifyStore.legalEntity, data, driver);
		obj.check(uiVerifyStore.parentAgreement, data, driver);
		obj.check(uiVerifyStore.catalinaEntity, data, driver);
		obj.check(uiVerifyStore.totalAgreementValue, data, driver);
		obj.check(uiVerifyStore.catalinaEntityNotes, data, driver);
		obj.check(uiVerifyStore.primaryContact, data, driver);
		obj.check(uiVerifyStore.oneTimeAgreement, data, driver);
		obj.check(uiVerifyStore.requestor, data, driver);
		obj.check(uiVerifyStore.intakeDate, data, driver);
		obj.check(uiVerifyStore.owner, data, driver);
		obj.check(uiVerifyStore.customContract, data, driver);
		obj.check(uiVerifyStore.internalBusinessOwner, data, driver);
		obj.check(uiVerifyStore.description, data, driver);
		obj.check(uiVerifyStore.incompleteRequest, data, driver);
		obj.check(uiVerifyStore.legacyRecord, data, driver);
		obj.check(uiVerifyStore.incompleteRequestNotes, data, driver);
		excelData.readData("Sheet1");
		data = ExcelReadWrite.list.get("Catalina Entity");
		System.out.println(data.toString());
		List<String> temp = new ArrayList<String>();
		temp = helper.dropDown(uiVerifyStore.catalinaEntityDropDown, driver);
		temp.remove(0);
		System.out.println(temp.toString());
		if (temp.equals(data)) {
			System.out.println("The drop down of Catalina Entity is correct and verified");
		} else {
			System.out.println("The drop down of Catalina Entity is not correct");
		}

		return driver;
	}

	public void check(By item, List<String> data, WebDriver driver) {
		String str = driver.findElement(item).getText();
		str = str.replaceAll("[*]", "");
		str = str.replaceAll("(?m)^\\s*$[\n\r]{1,}", "");
		System.out.println(str);
		if (data.contains(str)) {
			System.out.println("The value with this " + str + " field is avilable in web page");
			Reporter.addStepLog("The value with this " + str + " field is avilable in web page");
		} else {
			System.out.println("The value with this " + str + " field is not avilable in web page");
			Reporter.addStepLog("The value with this " + str + " field is not avilable in web page");
		}
	}

	public WebDriver termTermination(String key, WebDriver driver) throws IOException {
		List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.agreementStartDate, data, driver);
		obj.check(uiVerifyStore.autoRenew, data, driver);
		obj.check(uiVerifyStore.perpetual, data, driver);
		obj.check(uiVerifyStore.terminationConvenience, data, driver);
		obj.check(uiVerifyStore.agreementEndDate, data, driver);
		obj.check(uiVerifyStore.terminationDate, data, driver);
		obj.check(uiVerifyStore.amendmentEffectiveDate, data, driver);
		obj.check(uiVerifyStore.terminationComments, data, driver);
		obj.check(uiVerifyStore.terminationFees, data, driver);
		obj.check(uiVerifyStore.transitionServices, data, driver);
		obj.check(uiVerifyStore.terminationFeesNotes, data, driver);
		obj.check(uiVerifyStore.transitionServicesNotes, data, driver);
		return driver;
	}
	public WebDriver paymentTerm(String key,WebDriver driver) throws IOException
	{
		List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.paymentTerms, data, driver);
		obj.check(uiVerifyStore.paymentTermsNotes, data, driver);
		obj.check(uiVerifyStore.catsMa, data, driver);
		obj.check(uiVerifyStore.catsProduct, data, driver);
		obj.check(uiVerifyStore.catsContract, data, driver);
		obj.check(uiVerifyStore.catsBilling, data, driver);
		return driver;
	}
   public WebDriver litigation(String key,WebDriver driver) throws IOException
   {
	   List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.arbitration, data, driver);
		obj.check(uiVerifyStore.litigation, data, driver);
		obj.check(uiVerifyStore.governingLaw, data, driver);
		obj.check(uiVerifyStore.fourmSelection, data, driver);
		obj.check(uiVerifyStore.governingLawNotes, data, driver);
		obj.check(uiVerifyStore.fourmSelectionNotes, data, driver);
	   return driver;
   }
   public WebDriver specialDamage(String key,WebDriver driver) throws IOException
   {
	    List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.generalDamage, data, driver);
		obj.check(uiVerifyStore.specialDamage, data, driver);
		obj.check(uiVerifyStore.generalDamageNotes, data, driver);
		obj.check(uiVerifyStore.specialDamageNotes, data, driver);
		obj.check(uiVerifyStore.damageWaiver, data, driver);
		obj.check(uiVerifyStore.damageWaiverNotes, data, driver);
	   return driver;
   }
   public WebDriver indemnificationScope(String key,WebDriver driver) throws IOException
   {
	   List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.indemnificationCatalina, data, driver);
		obj.check(uiVerifyStore.indemnificationScope, data, driver);
		obj.check(uiVerifyStore.indemnification, data, driver);
		obj.check(uiVerifyStore.indemnificationNotes, data, driver);
		obj.check(uiVerifyStore.mutualIndemnification, data, driver); 
	   return driver;
   }
   public WebDriver miscellaneous(String key,WebDriver driver) throws IOException
   {
	   List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.auditRights, data, driver);
		obj.check(uiVerifyStore.catalinaAssignment, data, driver);
		obj.check(uiVerifyStore.auditRightsNotes, data, driver);
		obj.check(uiVerifyStore.catalinaAssignmentNotes, data, driver);
		obj.check(uiVerifyStore.nonCompete, data, driver);
		obj.check(uiVerifyStore.otherPartyAssignment, data, driver);
		obj.check(uiVerifyStore.nonCompeteNotes, data, driver);
		obj.check(uiVerifyStore.otherPartyAssignmentNotes, data, driver);
		obj.check(uiVerifyStore.mostFavoredNations, data, driver);
		obj.check(uiVerifyStore.employeeSolicitation, data, driver);
		obj.check(uiVerifyStore.mostFavoredNationsNotes, data, driver);
		obj.check(uiVerifyStore.employeeSolicitationNotes, data, driver);
		obj.check(uiVerifyStore.specialRequirements, data, driver);
		obj.check(uiVerifyStore.subcontractorRestriction, data, driver);
		obj.check(uiVerifyStore.digitalNetwork, data, driver);
		obj.check(uiVerifyStore.subcontractorRestrictionNotes, data, driver);
		obj.check(uiVerifyStore.buyerVision, data, driver);
		obj.check(uiVerifyStore.additionalCustomTerms, data, driver);
		obj.check(uiVerifyStore.legalApprover, data, driver);
		return driver;
   }
   public WebDriver signatures(String key,WebDriver driver) throws IOException
   {
	   List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.catalinaSigned, data, driver);
		obj.check(uiVerifyStore.clientSigned, data, driver);
		obj.check(uiVerifyStore.catalinaSignedDate, data, driver);
		obj.check(uiVerifyStore.clientSignedDate, data, driver);
		obj.check(uiVerifyStore.source, data, driver);
	return driver;	
   }
   public WebDriver systemInformation(String key,WebDriver driver) throws IOException
   {
	   List<String> data = new ArrayList<String>();
		excelData.readData("Sheet2");
		data = ExcelReadWrite.list.get(key);
		ApttusVerifyFieldsMethod obj = new ApttusVerifyFieldsMethod();
		obj.check(uiVerifyStore.isInternalReview, data, driver);
		return driver;
   }
}
