package com.mindtree.pagemethods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.mindtree.reusablecomponents.HelperClass;
import com.mindtree.uistore.ApttusPositiveFlowFillDetailsUistore;
import com.mindtree.uistore.ApttusPositiveFlowUistore;
import com.mindtree.uistore.ApttusPositiveFlowVerifyuiStore;
import com.mindtree.utility.ExcelReadWrite;

public class ApttusPositiveFlowMethods {

	HelperClass helper = new HelperClass();

	WebDriver driver;

	ExcelReadWrite excelData = new ExcelReadWrite();

	public ApttusPositiveFlowMethods(WebDriver driver) {
		super();
		this.driver = driver;
	}

	ApttusPositiveFlowUistore uiStore = new ApttusPositiveFlowUistore(driver);

	ApttusPositiveFlowVerifyuiStore uiVerifyStore = new ApttusPositiveFlowVerifyuiStore(driver);

	ApttusPositiveFlowFillDetailsUistore uiFillStore = new ApttusPositiveFlowFillDetailsUistore(driver);

	public WebDriver chooseAgreement(String key, WebDriver driver) throws IOException {
		HashMap<String, String> map = new HashMap<String, String>();

		List<String> list = new ArrayList<String>();

		excelData.readTestData("Apttus");

		if (ExcelReadWrite.map.containsKey(key)) {
			map = ExcelReadWrite.map.get(key);

			list = helper.dropDown(uiStore.agreementRecord, driver);

			if (list.contains(map.get("Agreement Type"))) {
				helper.insertValue(uiStore.agreementRecord, driver, map.get("Agreement Type"));
			} else {
				Reporter.addStepLog("The Requried data to Select Agreement Record Type is not present in drop down");
			}

		}
		return driver;
	}

	public WebDriver fillDetails(String key, WebDriver driver) throws IOException {
		HashMap<String, String> map = new HashMap<String, String>();
		excelData.readTestData("Apttus");

		if (ExcelReadWrite.map.containsKey(key)) {
			map = ExcelReadWrite.map.get(key);
			if (key.equalsIgnoreCase("Vendor Consulting SOW layouts")) {
				driver = details(driver, map);
			} else {
				driver.findElement(uiFillStore.productName).sendKeys(map.get("Product Name"));
				driver = details(driver, map);
			}

		}

		return driver;
	}

	private WebDriver details(WebDriver driver, HashMap<String, String> map) {
		List<String> list = new ArrayList<String>();
		driver.findElement(uiFillStore.vendorNotes).sendKeys(map.get("Vendor Notes"));
		driver.findElement(uiFillStore.clickProductSearch).click();
		ArrayList<String> tab2 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab2.get(1));
		driver.manage().window().maximize();
		driver.switchTo().frame("searchFrame");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(uiFillStore.enterSearch).sendKeys(map.get("Account"));
		driver.findElement(uiFillStore.clickSearch).click();
		driver.switchTo().parentFrame();
		driver.switchTo().frame("resultsFrame");
		driver.findElement(uiFillStore.accountSelect).click();
		driver.switchTo().window(tab2.get(0));
		driver.findElement(uiFillStore.vendorContact).sendKeys(map.get("Vendor Contact Information"));
		driver.findElement(uiFillStore.internalNotes).sendKeys(map.get("Internal Notes"));
		driver.findElement(uiFillStore.internalBusinessOwner).click();
		ArrayList<String> tab3 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tab3.get(1));
		driver.manage().window().maximize();
		driver.switchTo().frame("resultsFrame");
		driver.findElement(uiFillStore.internalBusinessOwnerSelect).click();
		driver.switchTo().window(tab3.get(0));
		driver.findElement(uiFillStore.totalAgreementValue).sendKeys(map.get("Total Agreement Value"));
		driver.findElement(uiFillStore.contractTotalYear).sendKeys(map.get("Contract Total Year # 1"));
		driver.findElement(uiFillStore.numberOfMilestones).sendKeys(map.get("Number of Milestones"));
		list = helper.dropDown(uiFillStore.paymentType, driver);
		if (list.contains(map.get("Payment Type"))) {
			driver = helper.insertValue(uiFillStore.paymentType, driver, map.get("Payment Type"));
			driver.findElement(uiFillStore.costCenter).sendKeys(map.get("Cost Center Manager"));
			driver.findElement(uiFillStore.quantity).sendKeys(map.get("Quantity"));
			driver.findElement(uiFillStore.costCenterManager).click();
			ArrayList<String> tab4 = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab4.get(1));
			driver.manage().window().maximize();
			driver.switchTo().frame("resultsFrame");
			driver.findElement(uiFillStore.internalBusinessOwnerSelect).click();
			driver.switchTo().window(tab4.get(0));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("document.getElementById('00NF000000DZ7LQ').value='" + helper.getDate(Integer.parseInt(map.get("Agreement Start Date"))) + "'");
			js.executeScript("document.getElementById('00NF000000DZ7LO').value='" + helper.getDate(Integer.parseInt(map.get("Agreement End Date"))) + "'");
			driver.findElement(uiFillStore.save).click();
		} else {
			Reporter.addStepLog("The Requried data to Select Payment Type is not present in drop down");
		}
		return driver;
	}

}
