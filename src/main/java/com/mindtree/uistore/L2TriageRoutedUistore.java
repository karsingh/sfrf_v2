package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author M1048963 date:2/13/2019 store locators
 *
 */
public class L2TriageRoutedUistore {

	WebDriver driver;

	public L2TriageRoutedUistore(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public By username = By.xpath("//input[@id='username']");

	public By password = By.xpath("//input[@id='password']");

	public By login = By.xpath("//input[@id='Login']");

	public By requestSupportUs = By.xpath(
			"//div[@id='rightPanel-body']//div[@id='tab-title-item' and contains(text(),'Request support with an Application (US, CR & JP)')]");

	
}
