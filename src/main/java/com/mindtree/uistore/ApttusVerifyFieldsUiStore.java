package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApttusVerifyFieldsUiStore {

	WebDriver driver;

	public ApttusVerifyFieldsUiStore(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public By statusCategory = By.xpath("//select[@id='00NF000000DZ7MJ']");

	public By status = By.xpath("//select[@id='00NF000000DZ7MK']");

	public By agreementName = By.xpath("//label[@for='Name']");

	public By recordType = By.xpath("//label[contains(text(),'Record Type')]");

	public By account = By.xpath("//label[contains(text(),'Account')]");

	public By legalEntity = By.xpath("//label[contains(text(),'Legal Entity')]");

	public By parentAgreement = By.xpath("//label[contains(text(),'Parent Agreement')]");

	public By catalinaEntity = By.xpath("//label[contains(text(),'Catalina Entity')]");

	public By catalinaEntityDropDown = By.xpath("//select[@id='00N2A00000DGBa9']");

	public By totalAgreementValue = By.xpath("//label[contains(text(),'Total Agreement Value')]");

	public By catalinaEntityNotes = By.xpath("//label[contains(text(),'Catalina Entity Notes/Detail')]");

	public By primaryContact = By.xpath("//label[contains(text(),'Primary Contact')]");

	public By requestor = By.xpath("//label[contains(text(),'Requestor')]");

	public By intakeDate = By.xpath("//label[contains(text(),'Intake Date')]");

	public By oneTimeAgreement = By.xpath("//label[contains(text(),'One-Time Agreement')]");

	public By owner = By.xpath("//label[contains(text(),'Owner')]");

	public By customContract = By.xpath("//label[contains(text(),'Custom Contract')]");

	public By internalBusinessOwner = By.xpath("//label[contains(text(),'Internal Business Owner')]");

	public By description = By.xpath("//label[contains(text(),'Description')]");

	public By incompleteRequest = By.xpath("//label[contains(text(),'Incomplete Request')]");

	public By legacyRecord = By.xpath("//label[contains(text(),'Transition/Legacy Record')]");

	public By incompleteRequestNotes = By.xpath("//label[contains(text(),'Incomplete Request Notes/Detail')]");

	public By message = By.xpath("//img[@src='/img/s.gif'][position()=27]");

	public By agreementStartDate = By.xpath("//label[contains(text(),'Agreement Start Date')]");

	public By autoRenew = By.xpath("//label[contains(text(),'Auto Renew')]");

	public By perpetual = By.xpath("//label[contains(text(),'Perpetual')]");

	public By terminationConvenience = By.xpath("//label[contains(text(),'Termination for Convenience')]");

	public By agreementEndDate = By.xpath("//label[contains(text(),'Agreement End Date')]");

	public By terminationDate = By.xpath("//label[contains(text(),'Termination Date')]");

	public By amendmentEffectiveDate = By.xpath("//label[contains(text(),'Amendment Effective Date')]");

	public By terminationComments = By.xpath("//label[contains(text(),'Termination Comments')]");

	public By terminationFees = By.xpath("//label[contains(text(),'Termination Fees')]");

	public By transitionServices = By.xpath("//label[contains(text(),'Transition Services')]");

	public By terminationFeesNotes = By.xpath("//label[contains(text(),'Termination Fees Notes/Detail')]");

	public By transitionServicesNotes = By.xpath("//label[contains(text(),'Transition Services Notes/Detail')]");

	public By paymentTerms = By.xpath("//label[@for='00NF000000DZ7Yi']");

	public By paymentTermsNotes = By.xpath("//label[@for='00N2A00000Dk9PX']");

	public By catsMa = By.xpath("//label[contains(text(),'CATS MA #')]");

	public By catsContract = By.xpath("//label[contains(text(),'CATS Contract #')]");

	public By catsBilling = By.xpath("//label[contains(text(),'CATS Billing #')]");

	public By catsProduct = By.xpath("//label[contains(text(),'CATS Product #')]");

	public By arbitration = By.xpath("//label[contains(text(),'Arbitration')]");
	
	public By litigation = By.xpath("//label[contains(text(),'Litigation')]");
	
	public By governingLaw = By.xpath("//label[contains(text(),'Governing Law')]");

	public By fourmSelection = By.xpath("//label[contains(text(),'Forum Selection')]");

	public By governingLawNotes = By.xpath("//label[contains(text(),'Governing Law Notes/Details')]");

	public By fourmSelectionNotes = By.xpath("//label[contains(text(),'Forum Selection Notes/Details')]");
	
	public By generalDamage = By.xpath("//label[contains(text(),'General Damages Cap')]");
	
	public By specialDamage = By.xpath("//label[contains(text(),'Special Damages Cap')]");

	public By generalDamageNotes = By.xpath("//label[contains(text(),'General Damage Cap Notes/Detail')]");

	public By specialDamageNotes = By.xpath("//label[contains(text(),'Special Damages Cap Notes/Detail')]");

	public By damageWaiver = By.xpath("//label[contains(text(),'Damage Waiver')]");

	public By damageWaiverNotes = By.xpath("//label[contains(text(),'Damage Waiver Notes/Detail')]");
	
	public By indemnification = By.xpath("//label[contains(text(),'Indemnification by Other Party')]");
	
	public By indemnificationNotes = By.xpath("//label[contains(text(),'Indemnification Notes/Details')]");
	
	public By mutualIndemnification = By.xpath("//label[contains(text(),'Mutual Indemnification')]");
	
	public By indemnificationCatalina = By.xpath("//label[contains(text(),'Indemnification by Catalina Party')]");
	
	public By indemnificationScope = By.xpath("//label[contains(text(),'Indemnification Scope')]");
	
	public By auditRights = By.xpath("//label[contains(text(),'Audit Rights')]");
	
	public By catalinaAssignment = By.xpath("//label[contains(text(),'Catalina Assignment')]");
	
	public By auditRightsNotes = By.xpath("//label[contains(text(),'Audit Rights Notes/Detail')]");
	
	public By catalinaAssignmentNotes = By.xpath("//label[contains(text(),'Catalina Assignment Notes/Detail')]");
	
	public By nonCompete = By.xpath("//label[contains(text(),'Non-Compete/Non-Solicit')]");
	
	public By otherPartyAssignment = By.xpath("//label[contains(text(),'Otherparty Assignment')]");
	
	public By nonCompeteNotes = By.xpath("//label[contains(text(),'Non-Compete/Non-Solicit Notes/Details')]");
	
	public By otherPartyAssignmentNotes = By.xpath("//label[contains(text(),'Otherparty Assignment Notes/Detail')]");
	
	public By mostFavoredNations = By.xpath("//label[contains(text(),'Most Favored Nations Pricing')]");
	
	public By employeeSolicitation = By.xpath("//label[contains(text(),'Employee Non-Solicitation')]");
	
	public By mostFavoredNationsNotes = By.xpath("//label[contains(text(),'Most Favored Nations Pricing Notes')]");
	
	public By employeeSolicitationNotes = By.xpath("//label[contains(text(),'Employee Non-Solicitation Notes')]");
	
	public By specialRequirements = By.xpath("//label[contains(text(),'Special Requirements')]");
	
	public By subcontractorRestriction = By.xpath("//label[contains(text(),'Subcontractor Restriction')]");
	
	public By digitalNetwork = By.xpath("//label[contains(text(),'Digital Network T&Cs')]");
	
	public By subcontractorRestrictionNotes = By.xpath("//label[contains(text(),'Subcontractor Restriction Notes')]");
	
	public By buyerVision = By.xpath("//label[contains(text(),'BuyerVision T&Cs')]");
	
	public By additionalCustomTerms = By.xpath("//label[contains(text(),'Additional Custom Terms & Conditions')]");
	
	public By legalApprover = By.xpath("//label[contains(text(),'Legal Approver')]");

	public By catalinaSigned = By.xpath("//label[contains(text(),'Catalina Signed By')]");
	
	public By clientSigned = By.xpath("//label[contains(text(),'Client Signed By')]");
	
	public By catalinaSignedDate = By.xpath("//label[contains(text(),'Catalina Signed Date')]");
	
	public By clientSignedDate = By.xpath("//label[contains(text(),'Client Signed Date')]");
	
	public By source = By.xpath("//label[contains(text(),'Source')]");
	
	public By isInternalReview = By.xpath("//*[@id=\"ep\"]/div[2]/div[21]/table/tbody/tr/td[3]");

}
