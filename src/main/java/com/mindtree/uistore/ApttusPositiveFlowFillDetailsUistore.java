package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApttusPositiveFlowFillDetailsUistore {

	WebDriver driver;

	public ApttusPositiveFlowFillDetailsUistore(WebDriver driver) {
		this.driver = driver;
	}

	public By productName = By.xpath("//textarea[@id='00NF000000DZNqt']");

	public By vendorNotes = By.xpath("//textarea[@id='00NF000000DZRGV']");

	public By clickProductSearch = By.xpath("//img[@class='lookupIcon'][position()=1]");

	public By enterSearch = By.xpath("//input[@id='lksrch']");

	public By clickSearch = By.xpath("//input[@value=' Go! ']");
	
	public By accountSelect=By.xpath("//a[@class=' dataCell ']");

	public By vendorContact = By.xpath("//textarea[@id='00NF000000DZ7Yh']");

	public By internalNotes = By.xpath("//textarea[@id='00N2A00000DpozP']");

	public By internalBusinessOwner = By.xpath("//img[@title='Internal Business Owner Lookup (New Window)']");

	public By internalBusinessOwnerSelect = By.xpath("//a[@class=' dataCell ']");

	public By totalAgreementValue = By.xpath("//input[@id='00NF000000DZ7MS']");

	public By contractTotalYear = By.xpath("//input[@id='00N2A00000DpozN']");

	public By numberOfMilestones = By.xpath("//input[@id='00N2A00000DpozQ']");

	public By paymentType = By.xpath("//select[@id='00N2A00000DGLks']");

	public By quantity = By.xpath("//input[@id='00N2A00000DpozR']");
	
	public By costCenter=By.xpath("//input[@id='00N2A00000DpozO']");

	public By costCenterManager = By.xpath("//img[@title='Cost Center Manager Lookup (New Window)']");

	public By agreementStartDate = By.xpath("//input[@id='00NF000000DZ7LQ']");

	public By agreementEndDate = By.xpath("//input[@id='00NF000000DZ7LO']");
	
	public By save=By.xpath("//input[@value=' Save '][position()=1]");

}
