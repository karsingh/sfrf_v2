package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApttusPositiveFlowVerifyuiStore {

	WebDriver driver;

	public ApttusPositiveFlowVerifyuiStore(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public By productName = By.xpath("//div[@id='00NF000000DZNqt_ileinner']");

	public By vendorNotes = By.xpath("//div[@id='00NF000000DZRGV_ileinner']");

	public By account = By.xpath("//div[@id='CF00NF000000DZ7L8_ileinner']");

	public By vendorContact = By.xpath("//div[@id='00NF000000DZ7Yh_ileinner']");

	public By internalNotes = By.xpath("//div[@id='00N2A00000DpozP_ileinner']");

	public By totalAgreementValue = By.xpath("//div[@id='00NF000000DZ7MS_ileinner']");

	public By contractTotalYear = By.xpath("//div[@id='00N2A00000DpozN_ileinner");

	public By numberofMilestone = By.xpath("//div[@id='00N2A00000DpozQ_ileinner']");

	public By costCenter = By.xpath("//div[@id='00N2A00000DpozO_ileinner']");

	public By quantity = By.xpath("//div[@id='00N2A00000DpozR_ileinner']");

	public By startDate = By.xpath("//div[@id='00NF000000DZ7LQ_ileinner']");

	public By endDate = By.xpath("//div[@id='00NF000000DZ7LO_ileinner']");

}
