package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TestDropDownUiStore {

	WebDriver driver;

	public TestDropDownUiStore(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public By clickonDropDown = By.xpath("//*[@id=\"AllTab_Tab\"]/a");

	public By clickRemedyConsole = By.xpath("//*[@id=\"bodyCell\"]/div[3]/div[2]/table/tbody/tr[17]/td[2]/a");
			
	public By clickNew = By.xpath("//span[@id='ListViewNewButton-btnInnerEl']");

	public By clickCategory = By
			.xpath("//a[@id='thpage:theForm:thePAgeBlock:j_id82:1:pageSectionId:j_id84:4:j_id85:inputField_lkwgt']");
}
