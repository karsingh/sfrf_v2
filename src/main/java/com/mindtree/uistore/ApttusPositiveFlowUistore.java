package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApttusPositiveFlowUistore {
	
	WebDriver driver;

	public ApttusPositiveFlowUistore(WebDriver driver) {
		this.driver = driver;
	}
	
	public By clickonDropDown=By.xpath("//*[@id=\"AllTab_Tab\"]/a");
	
	public By apttusMang=By.xpath("//a[@href='/a9W/o']");
	
	public By clickNew=By.xpath("//input[@name='new']");
	
	public By agreementRecord=By.xpath("//select[@id='p3']");
	
	public By continueFirst=By.xpath("//input[@value='Continue']");
	
	public By continueSecond=By.xpath("//input[@name='j_id0:j_id1:j_id2:j_id3:j_id4']");
	
	

}
