package com.mindtree.uistore;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class L2TriageRoutedFillDetailsUistore {

	WebDriver driver;

	public L2TriageRoutedFillDetailsUistore(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public By country = By.xpath("//select[@id='selectOption_a5VF0000000TOVVMA4']");

	public By application = By.xpath("//select[@id='selectOption_a5VF0000000TOVQMA4']");

	public By support = By.xpath("//select[@id='selectOption_a5VF0000000GrGXMA0']");

	public By description = By
			.xpath("//textarea[@id='thepage:incidentForm:pBlock:j_id132:j_id133:requestTableRepeater:13:textArea']");

	public By applicationVersion = By.xpath(
			"//input[@id='thepage:incidentForm:pBlock:j_id132:j_id133:requestTableRepeater:10:colTwotextfield']");

	public By submit = By.xpath("//button[@id='submitbtn'][position()=1]");

	public By alert = By.xpath("//span[@id='button-1006-btnInnerEl']");

	public By tickedNumber = By.xpath("//span[@id='tab-1027-btnInnerEl']");

	public By searchBox = By.xpath("//input[@id='phSearchInput']");

	public By clickSearchBox = By.xpath("//input[@id='phSearchButton']");

	public By clickTicketNumber = By.xpath("//a[contains(@href,'/a4r0U00000034kJ?srPos=0&srKp=a4r')]");

	public By queue = By.xpath("//div[@id='00NF000000D8FIs_ileinner']");

}
