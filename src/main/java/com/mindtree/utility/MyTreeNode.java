package com.mindtree.utility;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyTreeNode<T> implements Iterable<MyTreeNode<T>> {
	private T data = null;
	private List<MyTreeNode<T>> children = new ArrayList<MyTreeNode<T>>();
	private MyTreeNode<T> parent = null;
	private List<MyTreeNode<T>> elementsIndex=null;
	public MyTreeNode(T data) {
		this.data = data;
	}

	public void addChild(MyTreeNode<T> child) {
		child.setParent(this);
		this.children.add(child);
	}

	public void addChild(T data) {
		MyTreeNode<T> newChild = new MyTreeNode<T>(data);
		this.elementsIndex = new ArrayList<MyTreeNode<T>>();
		this.elementsIndex.add(this);
		this.addChild(newChild);
	}

	public void addChildren(List<MyTreeNode<T>> children) {
		for (MyTreeNode<T> t : children) {
			t.setParent(this);
		}
		this.children.addAll(children);
	}
	
	public void registerChildForSearch(MyTreeNode<T> node) {
		elementsIndex.add(node);
		if (parent != null)
			parent.registerChildForSearch(node);
	}


	public List<MyTreeNode<T>> getChildren() {
		return children;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	private void setParent(MyTreeNode<T> parent) {
		this.parent = parent;
	}

	public MyTreeNode<T> getParent() {
		return parent;
	}

	public Iterator<MyTreeNode<T>> iterator() {

		return null;
	}

	public boolean isRoot() {
		return parent == null;
	}

	public boolean isLeaf() {
		return children.size() == 0;
	}
	public int getLevel() {
		if (this.isRoot())
			return 0;
		else
			return parent.getLevel() + 1;
	}
	public MyTreeNode<T> findTreeNode(Comparable<T> cmp) {
		for (MyTreeNode<T> element : this.elementsIndex) {
			T elData = element.data;
			if (cmp.compareTo(elData) == 0)
				return element;
		}
		return null;
	}

	@Override
	public String toString() {
		return data != null ? data.toString() : "[data null]";
	}


}
