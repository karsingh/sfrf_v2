package com.mindtree.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReadWrite {

	public static void main(String args[]) throws IOException, ParseException {
		ExcelReadWrite obj = new ExcelReadWrite();
		obj.readData("Sheet1");
	}

	public static LinkedHashMap<String, HashMap<String, String>> map = new LinkedHashMap<String, HashMap<String, String>>();

	public static LinkedHashMap<String, List<String>> list = new LinkedHashMap<String, List<String>>();

	static String testDataPath = "C:\\Users\\M1048963\\Downloads\\Workspace1\\testinput\\TestData.xlsx";

	@SuppressWarnings("deprecation")
	public LinkedHashMap<String, HashMap<String, String>> readTestData(String sheetName) throws IOException {
		FileInputStream file = new FileInputStream(testDataPath);

		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheet(sheetName);

		Row headerRow = sheet.getRow(0);

		String str = null;

		for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
			Row currentRow = sheet.getRow(i);

			LinkedHashMap<String, String> currentMap = new LinkedHashMap<String, String>();

			for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++) {
				Cell currentCell = currentRow.getCell(0);

				switch (currentCell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					str = currentCell.getStringCellValue();
					str = str.trim();
					break;

				case Cell.CELL_TYPE_NUMERIC:
					str = String.valueOf(currentCell.getNumericCellValue()).trim();
					break;

				}
				Cell currentCell1 = currentRow.getCell(j);
				currentMap.put(headerRow.getCell(j).getStringCellValue().trim(),
						currentCell1.getStringCellValue().trim());
			}
			map.put(str, currentMap);
		}
		workbook.close();
		return map;

	}

	@SuppressWarnings("deprecation")
	public LinkedHashMap<String, List<String>> readData(String sheetName) throws IOException {
		FileInputStream file = new FileInputStream(testDataPath);

		XSSFWorkbook workbook = new XSSFWorkbook(file);

		XSSFSheet sheet = workbook.getSheet(sheetName);

		String str = null;

		for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
			Row currentRow = sheet.getRow(i);

			List<String> currentMap = new ArrayList<String>();

			for (int j = 1; j < currentRow.getPhysicalNumberOfCells(); j++) {
				Cell currentCell = currentRow.getCell(0);
				str = currentCell.getStringCellValue();
				currentCell = currentRow.getCell(j);
				switch (currentCell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					String temp = currentCell.getStringCellValue();
					temp = temp.trim();
					currentMap.add(temp);
					break;

				case Cell.CELL_TYPE_NUMERIC:
					temp = String.valueOf(currentCell.getNumericCellValue()).trim();
					break;

				}
			}
			list.put(str, currentMap);
			
		}
		System.out.println(list.toString());
		workbook.close();
		return list;
	}
}
