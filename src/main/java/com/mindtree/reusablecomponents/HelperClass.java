package com.mindtree.reusablecomponents;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author M1048963 date:2/13/2019
 *
 */
public class HelperClass {

	static WebDriver driver;

	/**
	 * @param url
	 * @return driver
	 */
	public WebDriver startBrowser(String url) {
		String exepath = "./Driver/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exepath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
	}

	/**
	 * @param driver
	 */
	public void closeUrl(WebDriver driver) {
		driver.quit();
	}

	/**
	 * @return
	 * @throws IOException
	 */
	public Properties propertyConfig() throws IOException {
		Properties properties = new Properties();
		FileInputStream fis = new FileInputStream("./config.properties");
		properties.load(fis);
		return properties;
	}

	/**
	 * @param item
	 * @return
	 */
	public WebElement waitForElement(By item) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(item));
		return element;
	}

	/**
	 * @param xpath
	 * @param driver
	 * @return
	 */
	public List<String> dropDown(By xpath, WebDriver driver) {
		List<WebElement> dropdown = new Select(driver.findElement(xpath)).getOptions();

		List<String> list = new ArrayList<String>();
		for (WebElement element : dropdown) {
			if (element.getAttribute("value") != " ")
				list.add(element.getText());
		}

		return list;
	}

	/**
	 * @param xpath
	 * @param driver
	 * @param value
	 * @return
	 */
	public WebDriver insertValue(By xpath, WebDriver driver, String value) {
		List<WebElement> options = new Select(driver.findElement(xpath)).getOptions();
		for (int i = 0; i < options.size(); i++) {
			String optionName = options.get(i).getText();
			if (optionName.equals(value)) {
				options.get(i).click();
				break;

			}
		}

		return driver;
	}

	/**
	 * @param numberOfDays
	 * @return
	 */
	public String getDate(int numberOfDays) {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, numberOfDays);
		Date dn = c.getTime();
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		String finalDate = format.format(dn);
		return finalDate;
	}

}
