package com.mindtree.runner;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "features", glue = { "com.mindtree.stepsdefinition" }, plugin = { "pretty",
		"html:target/site/cucumber-pretty", "json:target/cucumber.json",
		"com.cucumber.listener.ExtentCucumberFormatter:cucumberReports\\report.html" }, tags= {"@Test2"},monochrome = true, dryRun = false)
public class TestRunner {

	

	@AfterClass
	public static void reportSetup() {
		Reporter.loadXMLConfig("C:\\Users\\M1048963\\Downloads\\Workspace1\\src\\main\\resources\\extent-config.xml");
		Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		Reporter.setSystemInfo("64 Bit", "Windows 10");
		Reporter.setSystemInfo("2.53.0", "Selenium");
		Reporter.setSystemInfo("3.3.9", "Maven");
		Reporter.setSystemInfo("1.8.0_66", "Java Version");
		Reporter.setTestRunnerOutput("Cucumber JUnit Test Runner");
		
	}

}
